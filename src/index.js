import 'bootstrap/dist/css/bootstrap.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

class FormUser extends React.Component{

    constructor(){
        super();
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleOnChange = this.handleOnChange.bind(this)
    }

    handleOnChange(event){
       
        this.setState({
            [event.target.name]: event.target.value
        })
    }


    handleSubmit(event){
        event.preventDefault();
        //const data = new FormData(event.target);
        let fields = [
           {
                    name: event.target['firstname'].value,
                    lastname: event.target['lastname'].value,
                    birthdate: event.target['birthdate'].value,
                    email: event.target['email'].value,
                    jobtitle: event.target['jobtitle'].value,
                    experience: event.target['experience'].value               
           }
        ]

            console.log("Send Data");
            
           //console.log(data)
            fetch('https://my-json-server.typicode.com/anissoraya/fake-api-v2/profile',{
              method: 'PUT', 
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(fields)

              
            })
    }
    render(){
        return(
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-header">
                                <div className="card-title">Update Form</div>
                            </div>
                            <div className="card-body">
                            <form id="submit_job" onSubmit={this.handleSubmit} onChange={this.handleOnChange}>
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="firstname">Firstname</label>
                                            <input type="text" className="form-control" id="firstname"  name="firstname" placeholder="Enter FirstName" />
                                            
                                        </div>
                                        
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label htmlFor="last-name">LastName</label>
                                            <input type="text" className="form-control" name="lastname"  id="lastname" placeholder="Enter LastName"  />
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                <div className="form-group row">
                                    <div className="col-md-12">
                                        <label htmlFor="email">Email</label>
                                        <input type="email" className="form-control" name="email" id="email"   placeholder="Enter FirstName" />
                                        
                                    </div>
                                   
                                </div>

                                <div className="form-group">
                                    <label htmlFor="birthdate">Birth Date</label>
                                    <input type="date" className="form-control" name="birthdate" id="birthdate"  placeholder="Enter Birthdate" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="jobtitle">Preferred Job Title</label>
                                    <input type="text" className="form-control" name="jobtitle" id="jobtitle"   placeholder="Enter Job Title"/>
                                    
                                </div>
                                <div className="form-group">
                                    <label htmlFor="experience">Number of years of Experiences</label>
                                    <input type="number" className="form-control" name="experience" id="experience"  />
                                    
                                </div>
                                <button type="submit" className="btn btn-primary">Submit</button>
                            </form>
                            </div>
                        </div>

                    </div>
                </div>
               
            </div>
        )
    }
}

ReactDOM.render(<FormUser />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
